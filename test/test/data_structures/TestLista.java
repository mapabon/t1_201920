package test.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Lista;

public class TestLista
{
	private Lista lista;

	@Before
	public void setup()
	{
		lista = new Lista<String>();
	} 

	public void setup2()
	{
		for (int i = 0; i < 508; i++) 
		{
			lista.addFirst("Hola" + i);
		}
	}

	@Test 
	public void Lista()
	{
		assertTrue(lista!=null);
		assertEquals(0, lista.size());
	}

	@Test
	public void testAgregar()
	{
		for (int i = 0; i < 508; i++) 
		{
			lista.addFirst("Hola" + i);
		}
		assertTrue(!lista.isEmpty());
		assertEquals(508, lista.size());

	}

	@Test
	public void testGet()
	{
		setup2();
		for (int i = 506; i > 0; i--) 
		{
			assertEquals("Hola" + (507-i), lista.get(i));
		}

	}

	@Test
	public void testRemove()
	{
		setup2();
		for (int i = 0; i < 100; i++) 
		{
			
				lista.remove(i);
		}
		assertEquals(408, lista.size());

	}
}
