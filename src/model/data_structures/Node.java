package model.data_structures;

public class Node<E>
{
	private E elemento;
	private Node<E> siguiente;
	private Node<E> anterior;
	public Node(E elemento) {
		siguiente = null;
		anterior = null;
		this.elemento = elemento;
	}
	public E getElemento() {
		return elemento;
	}
	public void setElemento(E elemento) {
		this.elemento = elemento;
	}
	public Node<E> getSiguiente() {
		return siguiente;
	}
	public void setSiguiente(Node<E> siguiente) {
		this.siguiente = siguiente;
	}
	public Node<E> getAnterior() {
		return anterior;
	}
	public void setAnterior(Node<E> anterior) {
		this.anterior = anterior;
	}
	
	
	
}
