package model.data_structures;

public interface ILista<E> extends Iterable <E>
{

	public void addFirst(E item);
	public void remove (int pos);
	public E get (int pos);
	public int size();
	public boolean isEmpty();

}
