package model.data_structures;

import java.util.Iterator;

public class Lista<E> implements ILista<E> {
	private Node<E> first;
	private int size;

	public Lista() {
		first = null;
		size = 0;
	}


	public Iterator<E> iterator()
	{
		return new Iterator<E>()
		{
			Node<E> act = null;
			public boolean hasNext()
			{
				if(size==0)
					return false;
				if(act == null)
					return true;
				return act.getSiguiente() != null;
			}

			public E next()
			{
				if (act==null)
					act = first;
				else
					act = act.getSiguiente();
				return act.getElemento();
			}
		};
	}

	@Override
	public void addFirst(E item)
	{
		if (first == null) 
		{
			first = new Node<E>(item);
		} else
		{
			Node<E> act = new Node<E> (item);
			first.setAnterior(act);
			act.setSiguiente(first);
			first = act;
		}
		size++;

	}

	@Override
	public void remove(int pos)
	{
		if (pos == 0)
		{
			first = first.getSiguiente();
			first.setAnterior(null);
			size--;
		}
		else
		{
			Node<E> anterior = null;
			Node<E> act = first;
			Node<E> siguiente = first.getSiguiente();


			if (size >= pos)
			{
				for (int i = 0; i < size-1; i++) 
				{
					if (pos == i)
					{
						anterior = act.getAnterior();
						siguiente = act.getSiguiente();
						if(anterior != null)
						{
							anterior.setSiguiente(siguiente);
						}
						if(siguiente!=null)
						{
							siguiente.setAnterior(anterior);
						}

						size--;
						break;
					}
					act=act.getSiguiente();
				}
			}
		}
	}

	@Override
	public E get(int pos)
	{
		E rta = null;
		if (size < pos)
		{
			return null;
		}
		Node<E> act =first;
		for (int i = 0; i < size-1; i++) 
		{	
			if(i==pos)
			{
				rta = act.getElemento();
				break;
			}
			act = act.getSiguiente();
		}
		return rta;
	}

	@Override
	public int size() 
	{
		return size;
	}

	@Override
	public boolean isEmpty() 
	{
		return size==0;
	}

}
