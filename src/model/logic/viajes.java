package model.logic;

public class viajes
{
	private int idOrigen;
	
	private int idLlegada;
	
	private int mes;
	
	private double tiempo;
	
	private double desviacion;

	public viajes(int pOrigen, int pLlegada, int pMes, double pTiempo, double pDesviacion)
	{
		idOrigen = pOrigen;
		idLlegada = pLlegada;
		mes = pMes;
		tiempo = pTiempo;
		desviacion = pDesviacion;
	}
	
	public int getIdOrigen() {
		return idOrigen;
	}

	public int getIdLlegada() {
		return idLlegada;
	}

	public int getMes() {
		return mes;
	}

	public double getTiempo() {
		return tiempo;
	}

	public double getDesviacion() {
		return desviacion;
	}
}
