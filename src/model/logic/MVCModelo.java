package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.ILista;
import model.data_structures.Lista;

import com.opencsv.CSVReader;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private ILista<viajes> datos;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new Lista<viajes>();
	}


	public void carga(String archivoCsv)
	{
		int totalLineas =0;
		int mes = 0;
		try {
			CSVReader Csvr = new CSVReader(new FileReader(archivoCsv));
			String[] lector = new String[6];

			int contador1 = 0;
			int contador2 = 0;
			int contador3 = 0;
			while((lector = Csvr.readNext()) != null)
			{
				lector = Csvr.readNext();
				viajes viaje = new viajes(Integer.parseInt(lector[0]), Integer.parseInt(lector[1]), Integer.parseInt(lector[2]), Double.parseDouble(lector[3]), Double.parseDouble(lector[4]));
				totalLineas++;
				datos.addFirst(viaje);	
				mes = Integer.parseInt(lector[2]);
				if(1 == mes||mes -3 ==1)
				{
					contador1++;
				}
				else if(2==mes||mes-3==2)
				{
					contador2++;
				}
				else 
				{
					contador3++;
				}
			}
			if (mes == 03 || mes == 01 || mes == 02)
			{
				System.out.println("En el mes " + 1 + " hubo "  + contador1 + " viajes");
				System.out.println("En el mes " + 2 + " hubo "  + contador2 + " viajes");
				System.out.println("En el mes " + 3 + " hubo "  + contador3 + " viajes");
			}

			else
			{
				System.out.println("En el mes " + 4 + " hubo "  + contador1 + " viajes");
				System.out.println("En el mes " + 5 + " hubo "  + contador2 + " viajes");
				System.out.println("En el mes " + 6 + " hubo "  + contador3 + " viajes");
			}

				System.out.println("La cantidad total de datos es: " + totalLineas);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Lista<viajes> consultaViajes(int mes, int origen)
	{
		Lista<viajes> rta = new Lista<>();
		int viajesMes = 0;
		int viajesMesOrigen = 0;
		for (viajes viaje : datos) 
		{
			if (viaje.getMes() == mes)
			{
				viajesMes++;
			}
			if (viaje.getMes() == mes && viaje.getIdOrigen() == origen)
			{
				rta.addFirst(viaje);
				System.out.println("La zona de origen es: " + origen + ", su zona de destino es: "
						+ viaje.getIdLlegada() + ", su tiempo promedio es: " + viaje.getTiempo() +
						" y su desviacion estandar es: " + viaje.getDesviacion());
				viajesMesOrigen++;
			}
		}
		if(rta.isEmpty())
			System.out.println("No hay viajes registrados desde la zona de origen.");

		int totalViajes = datos.size();
		double porcentajeMes = ((double)viajesMes*100)/(double)totalViajes;
		double porcentajeMesOrigen = ((double)viajesMesOrigen*100)/(double)totalViajes;
		System.out.println("El total de viajes reportados en el semestre fue de: " + totalViajes +
				"\nEl total de viajes reportados para el mes " + mes + " fue de: " + viajesMes +
				" y su porcentaje con respecto al total de viajes del semestre fue: " + porcentajeMes + 
				"\nEl total de viajes reportados para el mes " + mes + " saliendo desde la zona " + origen  
				+ " fue: " + viajesMesOrigen + " y su porcentaje con respecto al total de viajes del semestre fue: "
				+ porcentajeMesOrigen );
		return rta;
	}





}
