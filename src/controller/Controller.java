package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		int dato = 0;
		int dato2 = 0;
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargar datos. Imprime la cantidad de viajes realizados por mes ");
				    modelo = new MVCModelo(); 
						 modelo.carga("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv") ;
						 modelo.carga("./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv") ;

						 break;

				case 2:
					System.out.println("--------- \nImprime los datos de cada viaje. ");
					System.out.println( "Ingresar el mes que desea buscar ");
					
					dato = lector.nextInt();
					System.out.println("Ingresar la zona de origen que desea buscar");
					dato2 = lector.nextInt();
					modelo.consultaViajes(dato, dato2);
					System.out.println("Dato agregado");
					break;

					
				case 3: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
